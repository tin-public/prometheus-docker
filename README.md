# Prometheus demo using container
## Requirement
- docker
- docker-compose
- Mac or Linux

## How to run
### Change sensitive data
Change your slack api in alertmanager-config/alertmanager.yml

### Update volume permission (Linux only)
```
sudo chown 65534:65534 alertmanager-data
sudo chown 472:472 grafana-data
```
### Run docker-compose
```
docker-compose up -d
```

## Experiment
### Check docker container running
Service|Url|Note
--|--|--
Prometheus|http://localhost:9090|
Alert Manager|http://localhost:9093|
Grafana|http://localhost:3000|Default account: admin/admin
cadvisor|http://localhost:8080|
### Check alert manager
Run
```
docker-compose stop grafana
```
wait for 1 minute. Then, a message should be sent to your slack channel

### Check Grafana
Rerun grafana container
```
docker-compose start grafana
```
#### Dashboard
Add dashboard 893 and check dashboard

#### Grafana alert
#### Add grafana notification channel with your slack api url
#### Add alert rules
Add query
```
sum(irate(container_cpu_usage_seconds_total{name="stress-con"}[5m])) by (name) * 100
```
Add alert with
```
evaluate every 10s for 30s
when avg() of query(A, 30s, now) is above 60
```
Run stress in stress-con container
```
docker exec stress-con stress --cpu 1 --timeout 2m
```
Wait a moment, a message should be sent to your slack channel. Then `RESOLVED` message will sent after stress finish.
